describe('Exercise', () => {
    it('Opens main page', () => {
      cy.visit('http://localhost:8080/#/root/plans/list')
    })
    it('Login on the application', () => {
      cy.get(':nth-child(2) > .form-control').clear().type('admin')
      
      cy.get('[style="position: relative"] > .form-control').clear().type('init')
      
      cy.get('.btn').click()
      
      //Plans tab
      cy.get('.active > a').click()
      cy.url().should('include', 'root/plans')
     
      //Keywords tab
      cy.get('body > div > header > div > nav > div > div.collapse.navbar-collapse > ul:nth-child(1) > li:nth-child(2) > a').click()
      cy.url().should('include', 'root/functions')
      cy.get('#DataTables_Table_1 > thead > tr.searchheader > th:nth-child(1) > second-header > st-custom-form-inputdropdown > inputdropdown > input', { timeout: 10000 }).should('be.visible')
      
      //Parameters tab
      cy.get('body > div > header > div > nav > div > div.collapse.navbar-collapse > ul:nth-child(1) > li:nth-child(3) > a').click()
      cy.url().should('include', 'root/parameters')
      cy.get('#DataTables_Table_2 > thead > tr.searchheader > th:nth-child(1) > second-header > inputdropdown > input', { timeout: 10000 }).should('be.visible')
      
      //Executions tab
      cy.get('body > div > header > div > nav > div > div.collapse.navbar-collapse > ul:nth-child(1) > li:nth-child(4) > a').click()
      cy.url().should('include', 'root/executions')
      cy.get('#DataTables_Table_3 > thead > tr.searchheader > th:nth-child(1) > second-header > inputdropdown > input', { timeout: 10000 }).should('be.visible')

      //Scheduler tab
      cy.get('body > div > header > div > nav > div > div.collapse.navbar-collapse > ul:nth-child(1) > li:nth-child(5) > a').click()
      cy.url().should('include', 'root/scheduler')
      cy.get('#DataTables_Table_4 > thead > tr.searchheader > th:nth-child(1) > second-header > st-custom-form-inputdropdown > inputdropdown > input', { timeout: 10000 }).should('be.visible')

      //Grid tab
      cy.get('body > div > header > div > nav > div > div.collapse.navbar-collapse > ul:nth-child(1) > li:nth-child(6) > a').click()
      cy.url().should('include', 'root/grid')
      cy.get('#DataTables_Table_5 > thead > tr.searchheader > th:nth-child(1) > second-header > inputdropdown > input', { timeout: 10000 }).should('be.visible')

      //Admin tab
      cy.get('body > div > header > div > nav > div > div.collapse.navbar-collapse > ul:nth-child(1) > li:nth-child(7) > a').click()
      cy.url().should('include', 'root/admin')
      cy.get('#DataTables_Table_6 > thead > tr.searchheader > th:nth-child(1) > second-header > inputdropdown > input', { timeout: 10000 }).should('be.visible')

    
    })

    
  })
