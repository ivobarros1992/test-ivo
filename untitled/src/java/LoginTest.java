import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class LoginTest {

    @Test
    public void login() {
       System.setProperty("webdriver.chrome.driver","C:\\Users\\Findmore Consulting\\Documents\\Projects\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("http://localhost:8080/#/root/plans/list");

        LoginPage loginPopup = new LoginPage(driver);
        loginPopup.insertUsername("admin");
        loginPopup.insertPassword("init");
        loginPopup.clickOnLoginButton();
        Assert.assertTrue(driver.getCurrentUrl().contains("plans"));

        Tabs tabs = new Tabs(driver);
        tabs.clickPlansTab();
        Assert.assertTrue(driver.getCurrentUrl().contains("plans"));
        tabs.clickKeywordsTab();
        Assert.assertTrue(driver.getCurrentUrl().contains( "functions"));
        tabs.clickParametersTab();
        Assert.assertTrue(driver.getCurrentUrl().contains("parameters"));
        tabs.clickExecutionsTab();
        Assert.assertTrue(driver.getCurrentUrl().contains("executions"));
        tabs.clickSchedulerTab();
        Assert.assertTrue(driver.getCurrentUrl().contains("scheduler"));
        tabs.clickGridTab();
        Assert.assertTrue(driver.getCurrentUrl().contains("grid"));
        tabs.clickAdminTab();
        Assert.assertTrue(driver.getCurrentUrl().contains( "admin"));


        // wait for grid to be present and visible

        driver.quit();
    }
}
