import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class Tabs {
    private WebDriver driver;
    private WebDriverWait wait;

    public Tabs(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.of(10, ChronoUnit.SECONDS));
    }

    public void clickPlansTab() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='collapse navbar-collapse']//ul//li//a[text()=' Plans']")));
        WebElement plansTab= driver.findElement(By.xpath("//div[@class='collapse navbar-collapse']//ul//li//a[text()=' Plans']"));
        plansTab.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='dataTables_length']//label[text()='Show ']")));
    }

    public void clickKeywordsTab() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='collapse navbar-collapse']//ul//li//a[text()=' Keywords']")));
        WebElement keywordsTab= driver.findElement(By.xpath("//div[@class='collapse navbar-collapse']//ul//li//a[text()=' Keywords']"));
        keywordsTab.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='dataTables_length']//label[text()='Show ']")));
    }

    public void clickParametersTab() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='collapse navbar-collapse']//ul//li//a[text()=' Parameters']")));
        WebElement parametersTab= driver.findElement(By.xpath("//div[@class='collapse navbar-collapse']//ul//li//a[text()=' Parameters']"));
        parametersTab.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='dataTables_length']//label[text()='Show ']")));
    }

    public void clickExecutionsTab() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='collapse navbar-collapse']//ul//li//a[text()=' Executions']")));
        WebElement parametersTab= driver.findElement(By.xpath("//div[@class='collapse navbar-collapse']//ul//li//a[text()=' Executions']"));
        parametersTab.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='dataTables_length']//label[text()='Show ']")));
    }

    public void clickSchedulerTab() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='collapse navbar-collapse']//ul//li//a[text()=' Scheduler']")));
        WebElement parametersTab= driver.findElement(By.xpath("//div[@class='collapse navbar-collapse']//ul//li//a[text()=' Scheduler']"));
        parametersTab.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='dataTables_length']//label[text()='Show ']")));
    }

    public void clickGridTab() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='collapse navbar-collapse']//ul//li//a[text()=' Grid']")));
        WebElement parametersTab= driver.findElement(By.xpath("//div[@class='collapse navbar-collapse']//ul//li//a[text()=' Grid']"));
        parametersTab.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='dataTables_length']//label[text()='Show ']")));
    }

    public void clickAdminTab() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='collapse navbar-collapse']//ul//li//a[text()=' Admin']")));
        WebElement parametersTab= driver.findElement(By.xpath("//div[@class='collapse navbar-collapse']//ul//li//a[text()=' Admin']"));
        parametersTab.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='dataTables_length']//label[text()='Show ']")));
    }
}
