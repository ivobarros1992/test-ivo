import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class LoginPage {
    private WebDriver driver;
    private WebDriverWait wait;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.of(10, ChronoUnit.SECONDS));
    }

    public void insertUsername(String username) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='form-group']//input[@name='username']")));
        WebElement usernameField = driver.findElement(By.xpath("//div[@class='form-group']//input[@name='username']"));
        usernameField.clear();
        usernameField.sendKeys(username);
    }

    public void insertPassword(String password) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='form-group']//input[@name='password']")));
        WebElement passwordField = driver.findElement(By.xpath("//div[@class='form-group']//input[@name='password']"));
        passwordField.clear();
        passwordField.sendKeys(password);
    }

    public void clickOnLoginButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='btn btn-lg btn-default btn-block']")));
        WebElement loginButton = driver.findElement(By.xpath("//button[@class='btn btn-lg btn-default btn-block']"));
        loginButton.click();
    }
}
